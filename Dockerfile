# Estágio de Build
FROM node:20-alpine as build
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# Estágio de Produção
FROM node:20-alpine
WORKDIR /app
# Copiando apenas a pasta build do estágio anterior
COPY --from=build /app/build ./build
# Instalando apenas as dependências de produção
COPY package*.json ./
RUN npm install --only=production
# Criando usuário não-root
RUN adduser -D myuser
USER myuser
# Expondo a porta
EXPOSE 3000
# Comando para iniciar o app
CMD ["node", "build/server.js"]
