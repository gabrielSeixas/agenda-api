
# Agenda API for Scheduling

## Overview

This is an API designed for a scheduling system, initially aimed at barbershops. The API provides features for user authentication, profile management, and will eventually include appointment scheduling functionalities.

## Architecture

The project follows a Hexagonal Architecture pattern, also known as Ports and Adapters. This architectural pattern helps in maintaining separation of concerns by dividing the application into several layers:

- **Domain**: Contains the core business logic and entities of the application.
- **Application**: Hosts the use-cases and DTOs. This is where the core functionalities are implemented.
- **Adapters**: Connects the application to external frameworks and libraries like Fastify and Knex.
- **Infrastructure**: Contains the setup and configuration for various infrastructure-related concerns.

### Directory Structure

```
src/
|-- adapters/
|   |-- controllers/
|   |-- middlewares/
|   |-- repositories/
|-- application/
|   |-- dtos/
|   |-- use_cases/
|   |-- errors/
|-- domain/
|   |-- entities/
|   |-- interfaces/
|-- infrastructure/
|   |-- express.ts
|   |-- knex.ts
|   |-- container.ts
|   |-- fastify.ts
|-- modules/
|   |-- auth/
```

## Development Guide

### Setting Up the Development Environment

1. Install Node.js and npm.
2. Run `npm install` to install the project dependencies.
3. Copy `.env.example` to `.env` and fill in the required environment variables.

### Adding a New Feature

1. Identify the layer(s) where the feature fits (Domain, Application, Adapters, etc.).
2. If a new repository is needed, start by creating an interface inside the `domain/interfaces` directory.
3. Implement the use-case inside the `application/use_cases` directory.
    - Use `tsyringe` for dependency injection within the use-case.
4. Create or update the adapter inside the `adapters` directory to expose the feature through an API.
5. Update the README.md and documentation as necessary.

### Running Tests

Run `npm test` to execute the test suite.

### Database Migrations

Run `npm run knex:migrate` to apply database migrations.

### Running the Server

Run `npm start` to start the Fastify server.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
