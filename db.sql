-- Table for Establishments
CREATE TABLE establishments (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    location VARCHAR(100)
);

-- Table for Branches
CREATE TABLE branches (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    establishment_id INT REFERENCES establishments(id),
    location VARCHAR(100)
);

-- Table for Services
CREATE TABLE services (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    description TEXT,
    price DECIMAL(10, 2)
);

-- Table for Professionals
CREATE TABLE professionals (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    branch_id INT REFERENCES branches(id)
);

-- Table for Appointments
CREATE TABLE appointments (
    id SERIAL PRIMARY KEY,
    service_id INT REFERENCES services(id),
    professional_id INT REFERENCES professionals(id),
    branch_id INT REFERENCES branches(id),
    appointment_time TIMESTAMPTZ NOT NULL
);

-- Table for Profiles
CREATE TABLE profiles (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    slug VARCHAR(50) UNIQUE NOT NULL,
    description TEXT
);

-- Table for Users
CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    username VARCHAR(50) UNIQUE NOT NULL,
    password_hash VARCHAR(128) NOT NULL,
    profile_id INT REFERENCES profiles(id)
);

-- Table for Permissions
CREATE TABLE permissions (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    description TEXT
);

-- Table for Profile_Permissions (N to N relationship)
CREATE TABLE profile_permissions (
    profile_id INT REFERENCES profiles(id),
    permission_id INT REFERENCES permissions(id),
    PRIMARY KEY (profile_id, permission_id)
);

-- Table for Professional_Services (N to N relationship)
CREATE TABLE professional_services (
    professional_id INT REFERENCES professionals(id),
    service_id INT REFERENCES services(id),
    PRIMARY KEY (professional_id, service_id)
);


insert into profiles (name, slug, description) values ('Administrador', 'admin', 'Administrador do sistema');
insert into profiles (name, slug, description) values ('Dono do Estabelecimento', 'establishments_owner', 'Dono do Estabelecimento');

