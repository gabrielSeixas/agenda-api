import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('services', table => {
        table.increments('id').primary();
        table.string('name').notNullable();
        table.integer('duration').notNullable();
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('services');
}
