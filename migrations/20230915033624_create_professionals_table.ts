import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('professionals', table => {
        table.increments('id').primary();
        table.string('name').notNullable();
        table.integer('establishment_id').references('id').inTable('establishments');
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('professionals');
}
