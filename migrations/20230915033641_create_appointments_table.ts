import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('appointments', table => {
        table.increments('id').primary();
        table.timestamp('appointment_time').notNullable();
        table.integer('user_id').references('id').inTable('users');
        table.integer('professional_id').references('id').inTable('professionals');
        table.integer('service_id').references('id').inTable('services');
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('appointments');
}
