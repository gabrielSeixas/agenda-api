import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('profiles_permissions', table => {
        table.integer('profile_id').references('id').inTable('profiles');
        table.integer('permission_id').references('id').inTable('permissions');
        table.primary(['profile_id', 'permission_id']);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('profiles_permissions');
}
