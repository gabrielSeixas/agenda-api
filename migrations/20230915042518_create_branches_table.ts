import { Knex } from "knex";

/*
* CREATE TABLE branches (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    establishment_id INT REFERENCES establishments(id),
    location VARCHAR(100)
);
* */

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('branches', table => {
        table.increments('id').primary();
        table.string('name').notNullable();
        table.integer('establishment_id').references('id').inTable('establishments');
        table.string('location');
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('branches');
}
