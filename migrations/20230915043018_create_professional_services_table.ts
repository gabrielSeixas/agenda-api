import { Knex } from "knex";

/*
* CREATE TABLE professional_services (
    professional_id INT REFERENCES professionals(id),
    service_id INT REFERENCES services(id),
    PRIMARY KEY (professional_id, service_id)
);
*/

export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('professional_services', table => {
        table.integer('professional_id').references('id').inTable('professionals');
        table.integer('service_id').references('id').inTable('services');
        table.primary(['professional_id', 'service_id']);
    });
}

export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('professional_services');
}
