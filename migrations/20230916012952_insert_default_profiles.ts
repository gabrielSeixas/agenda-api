import { type Knex } from 'knex';

export async function up (knex: Knex): Promise<void> {
  await knex.insert({
    name: 'Administrador',
    slug: 'admin',
    description: 'Administrador do sistema'
  }).into('profiles');

  await knex.insert({
    name: 'Dono do Estabelecimento',
    slug: 'establishments_owner',
    description: 'Dono do Estabelecimento'
  }).into('profiles');
}

export async function down (knex: Knex): Promise<void> {
  await knex('profiles').where({ slug: 'admin' }).del();
  await knex('profiles').where({ slug: 'establishments_owner' }).del();
}
