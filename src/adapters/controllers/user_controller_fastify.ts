import { type FastifyInstance } from 'fastify';
import { container } from '../../infrastructure/container';
import { CreateUserUseCase } from '../../application/use_cases/create_user';
import { HttpStatus } from '@decorators/server';
import { fastifyErrorHandler } from '../fastify_error_handler';
import { UserRepository } from '../repositories/user_repository_knex';
import { type UserPaginationDto } from '../../application/dtos/user_pagination_dto';
import { ListUserUseCase } from '../../application/use_cases/list_user';
import { type CreateUserDTO } from '../../application/dtos/create_user_dto';

export function userRoutes(app: FastifyInstance, options: Record<string, unknown>, done: (err?: Error) => void): void {
  app.post('/api/user', { preHandler: [] }, async (request, reply) => {
    const user = request.body as CreateUserDTO;
    const [error] = await container.resolve(CreateUserUseCase).execute(user);

    if (error != null) return fastifyErrorHandler(error, reply);

    void reply.status(HttpStatus.CREATED).send();
  });

  app.get('/api/user/:id', async (request, reply) => {
    const { id } = request.params as { id: number };

    const user = await container.resolve(UserRepository).findById(id, true);

    if (user == null) {
      void reply.status(HttpStatus.NOT_FOUND);
      return;
    }

    void reply.status(HttpStatus.OK);
    return user;
  });

  app.get('/api/user', async (request, reply) => {
    const query = request.query as UserPaginationDto;
    query.page = Number(query.page);
    query.limit = Number(query.limit);

    const [error, users] = await container.resolve(ListUserUseCase).execute(query);

    if (error != null) return fastifyErrorHandler(error, reply);

    void reply.status(HttpStatus.OK);
    return users;
  });

  done();
}
