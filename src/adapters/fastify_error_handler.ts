import { type FastifyReply as Response } from 'fastify';
import { HttpStatus } from '@decorators/server';
import { HttpError } from '../application/errors/http_error';

export function fastifyErrorHandler (error: Error, reply: Response): HttpError | Error {
  if (error instanceof HttpError) {
    void reply.status(error.status);
    try {
      error.cause = JSON.parse(error.message);
      void reply.send({
        status: error.status,
        message: error.cause
      });
    } finally {
      // eslint-disable-next-line
      return error
    }
  } else {
    void reply.status(HttpStatus.INTERNAL_SERVER_ERROR);
    return error;
  }
}
