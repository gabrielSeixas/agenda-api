import { type IProfileRepository } from '../../domain/interfaces/profile_repository';
import { type KnexConnection } from '../../infrastructure/knex';
import { inject, injectable } from 'tsyringe';
import { type Profile } from '../../domain/entities/profile';

@injectable()
export class ProfileRepositoryKnex implements IProfileRepository {
  constructor (
    @inject('KnexConnection') private readonly knex: KnexConnection
  ) {
  }

  public async getBySlug (slug: string): Promise<Profile | null> {
    const profile = await this.knex.instance()
      .select('*')
      .from<Profile>('profiles')
      .where({ slug })
      .first();

    return profile ?? null;
  }
}
