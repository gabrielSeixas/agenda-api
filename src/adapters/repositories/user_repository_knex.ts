import { type Knex } from 'knex';
import { inject, injectable } from 'tsyringe';
import { type User } from '../../domain/entities/user';
import { type IUserRepository } from '../../domain/interfaces/user_repository';
import { KnexConnection } from '../../infrastructure/knex';
import { type Profile } from '../../domain/entities/profile';

interface UserWithProfile extends User {
  profile: Profile
  name?: string
  description?: string
  slug?: string
}

@injectable()
export class UserRepository implements IUserRepository {
  constructor(
    @inject('KnexConnection') private readonly db: KnexConnection
  ) {
  }

  public async save(user: User): Promise<User> {
    return await this.db.instance().insert(user).into('users');
  }

  public async findById(id: number, withProfile?: boolean): Promise<User | null> {
    const query = this.db.instance()
      .select([
        'users.id',
        'users.name as userName',
        'users.username',
        'users.profile_id',
        'profiles.name as profileName',
        'profiles.slug',
        'profiles.description'
      ])
      .from<User>('users')
      .where('users.id', '=', id);

    return await this.buildProfileOnUser(query, withProfile);
  }

  public async findByUsername(username: string, withProfile?: boolean): Promise<User | null> {
    const profileAttrs = [
      'profiles.name as profileName',
      'profiles.slug',
      'profiles.description'
    ];

    const query = this.db.instance()
      .select([
        'users.id',
        'users.name as userName',
        'users.username',
        'users.password',
        'users.profile_id',
        ...(withProfile ?? false
          ? profileAttrs
          : [])
      ])
      .from<User>('users')
      .where({ username });

    return await this.buildProfileOnUser(query, withProfile);
  }

  public async listPaginated(page: number, limit: number, withProfiles?: boolean): Promise<User[]> {
    const users = await this.db.instance()
      .select('id', 'name', 'username')
      .from<User>('users')
      .offset((page - 1) * limit)
      .limit(limit);

    if (withProfiles === true) {
      await this.fetchProfilesByUsers(users);
    }

    return users;
  }

  private async buildProfileOnUser(query: Knex.QueryInterface, withProfile?: boolean): Promise<User | null> {
    if (withProfile ?? false) {
      void query.leftJoin<Profile>('profiles', 'users.profile_id', 'profiles.id');
    }

    const user = await query.first();

    if ((withProfile ?? false) && (user != null)) {
      this.structUserWithProfile(user as UserWithProfile);
    }

    if (user != null) return user;

    return null;
  }

  private async fetchProfilesByUsers(users: User[]): Promise<void> {
    const profiles = await this.db.instance()
      .select('*')
      .from<Profile>('profiles')
      .whereIn('id', users.map(user => user.profile_id as number));

    for (const user of users) {
      const profile = profiles.find(profile => profile.id === user.profile_id);
      (user as UserWithProfile).profile = profile as Profile;
    }
  }

  private structUserWithProfile(user: UserWithProfile & { profileName?: string, userName?: string }): void {
    user.name = (user).userName as string;
    user.profile = {} as Profile // eslint-disable-line

    user.profile = {
      id: (user).profile_id as number | null,
      name: (user).profileName as string,
      slug: (user).slug as string,
      description: (user).description as string
    };

    delete (user).userName;
    delete (user).profile_id;
    delete (user).profileName;
    delete (user).slug;
    delete (user).description;
  }
}
