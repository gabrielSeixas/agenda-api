import { z } from 'zod';

export const createUserDTOSchema = z.object({
  name: z.string().min(3).max(255),
  username: z.string().min(3).max(255).email(),
  password: z.string().min(3).max(255),
  confirmPassword: z.string().min(3).max(255)
}).refine(data => data.password === data.confirmPassword, {
  message: 'Passwords do not match',
  path: ['confirmPassword']
});

export type CreateUserDTO = z.infer<typeof createUserDTOSchema>;
