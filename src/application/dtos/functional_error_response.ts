export type FunctionalErrorResponse<T> = [Error | null, T | null]
