import { z } from 'zod';

export const userPaginationDtoSchema = z.object({
  page: z.number().int().positive().default(1),
  limit: z.number().int().positive().default(10),
  withProfiles: z.boolean().default(false).optional()
});

export type UserPaginationDto = z.infer<typeof userPaginationDtoSchema>
