export class HttpError extends Error {
  public cause: unknown;
  constructor (public readonly status: number, message: string) {
    super(message);
  }
}
