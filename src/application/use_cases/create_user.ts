import { z } from 'zod';
import { type User } from '../../domain/entities/user';
import HttpStatus from 'http-status-codes';
import argon2 from 'argon2';
import { inject, injectable } from 'tsyringe';

import { IUserRepository } from '../../domain/interfaces/user_repository';
import { HttpError } from '../errors/http_error';
import { type FunctionalErrorResponse } from '../dtos/functional_error_response';
import { type CreateUserDTO, createUserDTOSchema } from '../dtos/create_user_dto';
import { IProfileRepository } from '../../domain/interfaces/profile_repository';

@injectable()
export class CreateUserUseCase {
  constructor (
    @inject('IUserRepository') private readonly userRepository: IUserRepository,
    @inject('IProfileRepository') private readonly profileRepository: IProfileRepository
  ) {
  }

  async execute (data: CreateUserDTO): Promise<FunctionalErrorResponse<User>> {
    try {
      const validatedUser = createUserDTOSchema.parse(data) as User;
      const passwordHashPromise = argon2.hash(validatedUser.password);

      const establishmentOwnerProfile = await this.profileRepository.getBySlug('establishments_owner');

      if (establishmentOwnerProfile == null) {
        return [new HttpError(HttpStatus.NOT_FOUND, 'Profile not found'), null];
      }

      validatedUser.profile_id = establishmentOwnerProfile.id;
      validatedUser.password = await passwordHashPromise;
      delete (validatedUser as User & { confirmPassword?: string }).confirmPassword;
      await this.userRepository.save(validatedUser);

      return [null, validatedUser];
    } catch (error) {
      if (error instanceof z.ZodError) {
        return [new HttpError(HttpStatus.BAD_REQUEST, error.message), null];
      }

      if ((error as Error).message.includes('users_username_unique')) {
        return [new HttpError(HttpStatus.CONFLICT, 'Username already exists'), null];
      }

      return [error as Error, null];
    }
  }
}
