import { inject, injectable } from 'tsyringe';
import { IUserRepository } from '../../domain/interfaces/user_repository';
import { type FunctionalErrorResponse } from '../dtos/functional_error_response';

@injectable()
export class GenerateJWTTokenUseCase {
  constructor (
    @inject('IUserRepository') private readonly userRepository: IUserRepository
  ) {
  }

  public async execute (username: string, password: string): Promise<FunctionalErrorResponse<string>> {
    const userExists = await this.userRepository.findByUsername(username);

    if (userExists === false) {
      return [new Error('User does not exists'), null];
    }

    return [null, ''];
  }
}
