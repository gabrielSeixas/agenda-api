import { type UserPaginationDto, userPaginationDtoSchema } from '../dtos/user_pagination_dto';
import { HttpStatus } from '@decorators/server';
import { IUserRepository } from '../../domain/interfaces/user_repository';
import { z } from 'zod';
import { inject, injectable } from 'tsyringe';
import { type FunctionalErrorResponse } from '../dtos/functional_error_response';
import { HttpError } from '../errors/http_error';
import { type User } from '../../domain/entities/user';

@injectable()
export class ListUserUseCase {
  constructor (
    @inject('IUserRepository') private readonly userRepository: IUserRepository
  ) {}

  async execute (dto: UserPaginationDto): Promise<FunctionalErrorResponse<User[]>> {
    try {
      const { page, limit, withProfiles } = userPaginationDtoSchema.parse(dto);
      const users = await this.userRepository.listPaginated(page, limit, withProfiles);

      return [null, users];
    } catch (error) {
      if (error instanceof z.ZodError) {
        return [new HttpError(HttpStatus.BAD_REQUEST, error.message), null];
      } else {
        return [new HttpError(HttpStatus.INTERNAL_SERVER_ERROR, (error as Error).message), null];
      }
    }
  }
}
