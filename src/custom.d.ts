import 'fastify';
declare module 'fastify-rate-limit';
declare module 'fastify-swagger';
declare module 'fastify-cors';

declare module 'fastify' {
  interface FastifyRequest {
    user: {
      id: number
      username: string
      name: string
    }
  }
}
