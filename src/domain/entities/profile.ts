import { z } from 'zod';

export const profileSchema = z.object({
  id: z.number().int().positive().nullable(),
  name: z.string().min(3, { message: 'Name must be at least 3 characters long' }),
  slug: z.string().min(3, { message: 'Slug must be at least 3 characters long' }),
  description: z.string().min(3, { message: 'Description must be at least 3 characters long' })
});

export type Profile = z.infer<typeof profileSchema>
