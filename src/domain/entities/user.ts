import { z } from 'zod';
import { profileSchema } from './profile';

export const userSchema = z.object({
  id: z.number().int().positive().nullable().optional(),
  name: z.string().min(3, { message: 'username must have at least 3 characters' }),
  username: z.string().min(3, { message: 'username must have at least 3 characters' }),
  password: z.string().min(8, { message: 'password must have at least 8 characters' }),
  profile_id: z.number().int().positive().nullable().optional(),
  profile: profileSchema.optional()
});

export type User = z.infer<typeof userSchema>
