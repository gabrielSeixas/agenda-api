import { type Profile } from '../entities/profile';

export interface IProfileRepository {
  getBySlug: (slug: string) => Promise<Profile | null>
}
