import { type User } from '../entities/user';

export interface IUserRepository {
  save: (user: User) => Promise<User>
  findByUsername: (username: string, withProfile?: boolean) => Promise<User | null>
  findById: (id: number, withProfile?: boolean) => Promise<User | null>
  listPaginated: (page: number, limit: number, withProfiles?: boolean) => Promise<User[]>
}
