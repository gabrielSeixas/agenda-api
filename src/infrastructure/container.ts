import { container } from 'tsyringe';
import { UserRepository } from '../adapters/repositories/user_repository_knex';
import { CreateUserUseCase } from '../application/use_cases/create_user';
import { KnexConnection } from './knex';
import { ListUserUseCase } from '../application/use_cases/list_user';
import { ProfileRepositoryKnex } from '../adapters/repositories/profile_repository_knex';

import { LoginUseCase } from '../modules/auth/application/use_cases/login';

// Repositories
container.register('KnexConnection', { useClass: KnexConnection });
container.register('IUserRepository', { useClass: UserRepository });
container.register('IProfileRepository', { useClass: ProfileRepositoryKnex });

// UseCases
container.register('CreateUserUseCase', { useClass: CreateUserUseCase });
container.register('ListUserUseCase', { useClass: ListUserUseCase });
container.register('LoginUseCase', { useClass: LoginUseCase });

export { container };
