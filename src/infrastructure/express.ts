import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import morgan from 'morgan';

const { PORT = 3000 } = process.env;
const app = express();

// Middlewares
app.use(cors());
app.use(helmet());
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

export const router = express.Router();

export function startServer(): void {
  app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
  });
}
