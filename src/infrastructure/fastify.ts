import fastify from 'fastify';
import rateLimit from '@fastify/rate-limit';
import helmet from '@fastify/helmet';
import cors from '@fastify/cors';
import pino from 'pino';

import { userRoutes } from '../adapters/controllers/user_controller_fastify';
import { authRoutes } from '../modules/auth/adapters/auth_controller_fastify';

const { PORT = 3000, NODE_ENV = 'development' } = process.env;

export const app = fastify({
  logger: pino({ level: 'info' })
});

void app.register(helmet);
void app.register(cors);
void app.register(rateLimit, {
  max: 100,
  timeWindow: '1 minute'
});

app.setErrorHandler((error, request, response) => {
  request.log.error(error);
  void response.send(error);
});

void app.register(userRoutes);
void app.register(authRoutes);

export const startServer = async (): Promise<void> => {
  try {
    const port = Number(PORT);
    app.listen({ port, host: '0.0.0.0' }, (err, address) => {
      if (err != null) {
        NODE_ENV === 'development' ? console.error(err) : app.log.error(err);
        process.exit(1);
      }

      app.log.info(`app listening on ${address}`);
    });
  } catch (err) {
    app.log.error(err);
    process.exit(1);
  }
};
