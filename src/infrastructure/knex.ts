import knex, { type Knex } from 'knex';
import { injectable } from 'tsyringe';

@injectable()
export class KnexConnection {
  private readonly db: Knex<any, unknown[]>;

  constructor () {
    this.validateEnv();
    this.db = knex({
      client: 'pg',
      connection: {
        connectionString: process.env.POSTGRES_CONNECTION_URL,
        ssl: { rejectUnauthorized: false }
      },
      pool: {
        min: 2,
        max: 10
      }
    });
  }

  public instance (): Knex<any, unknown[]> {
    return this.db;
  }

  private validateEnv (): void {
    if (process.env.POSTGRES_DB_HOST == null) {
      throw new Error('POSTGRES_DB_HOST is not defined');
    }
    if (process.env.POSTGRES_DB_PORT == null) {
      throw new Error('POSTGRES_DB_PORT is not defined');
    }
    if (process.env.POSTGRES_DB_NAME == null) {
      throw new Error('POSTGRES_DB_NAME is not defined');
    }
    if (process.env.POSTGRES_DB_USER == null) {
      throw new Error('POSTGRES_DB_USER is not defined');
    }
    if (process.env.POSTGRES_DB_PASSWORD == null) {
      throw new Error('POSTGRES_DB_PASSWORD is not defined');
    }
  }
}
