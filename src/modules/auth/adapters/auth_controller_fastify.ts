import { type FastifyInstance } from 'fastify';
import { container } from '../../../infrastructure/container';
import { type LoginDto } from '../application/dtos/login_dto';
import { LoginUseCase } from '../application/use_cases/login';
import HttpStatus from 'http-status-codes';
import { HttpError } from '../../../application/errors/http_error';

export function authRoutes(app: FastifyInstance, options: Record<string, unknown>, done: (err?: Error) => void): void {
  app.post('/api/auth/login', async (request, reply) => {
    try {
      const { username, password } = request.body as LoginDto;

      const { token, user } = await container.resolve(LoginUseCase).execute({ username, password });

      void reply.code(HttpStatus.OK).send({ token, user });
    } catch (error) {
      if (error instanceof HttpError) {
        void reply.code(error.status).send({ error: error.message });
      }

      if (error instanceof Error) {
        void reply.code(HttpStatus.INTERNAL_SERVER_ERROR).send({ error: error.message });
      }
    }
  });

  done();
}
