import { type FastifyRequest as Req, type FastifyReply as Res } from 'fastify';
import jwt from 'jsonwebtoken';

interface LoggedUser {
  id: number
  username: string
  name: string
}

const { JWT_SECRET = 'secret' } = process.env;

export const authMiddleware = async (request: Req, reply: Res): Promise<void> => {
  const token = request.headers.authorization?.split(' ')[1];

  if (!token) { // eslint-disable-line
    void reply.status(401).send({ message: 'Token não fornecido!' });
    return;
  }

  try {
    const decoded = jwt.verify(token, JWT_SECRET) as LoggedUser;
    request.user = decoded;
  } catch (err) {
    void reply.status(401).send({ message: 'Token inválido!' });
  }
};
