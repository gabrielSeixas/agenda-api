import { sign } from 'jsonwebtoken';
import HttpStatus from 'http-status-codes';
import { type IUserRepository } from '../../../../domain/interfaces/user_repository';
import { comparePassword } from '../../utils/compare_password';
import { inject, injectable } from 'tsyringe';
import { HttpError } from '../../../../application/errors/http_error';
import { loginDto } from '../dtos/login_dto';

interface ILoginRequest {
  username: string
  password: string
}

interface LoginResponse {
  token: string
  user: {
    id: number
    name: string
    username: string
  }
}

const { JWT_SECRET = 'secret' } = process.env;

@injectable()
export class LoginUseCase {
  constructor(
    @inject('IUserRepository') private readonly userRepository: IUserRepository) { }

  async execute(data: ILoginRequest): Promise<LoginResponse> {
    const credentials = loginDto.parse(data);
    const user = await this.userRepository.findByUsername(credentials.username);

    if (user == null) {
      throw new HttpError(HttpStatus.NOT_FOUND, 'User not found');
    }

    const passwordMatch = await comparePassword(credentials.password, user.password);

    if (!passwordMatch) {
      throw new HttpError(HttpStatus.FORBIDDEN, 'Password does not match');
    }

    const token = sign(
      { userId: user.id, username: user.username },
      JWT_SECRET,
      {
        expiresIn: '1d'
      }
    );

    return {
      token,
      user: {
        id: user.id as number,
        username: user.username,
        name: user.name
      }
    };
  }
}
