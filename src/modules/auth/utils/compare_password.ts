import argon2 from 'argon2';

export async function comparePassword(plainPassword: string, hashedPassword: string): Promise<boolean> {
  try {
    const isMatch = await argon2.verify(hashedPassword, plainPassword);
    return isMatch;
  } catch (error) {
    throw new Error('Erro ao comparar as senhas, meu irmão!');
  }
};
