import 'reflect-metadata';
import 'dotenv/config';

import { startServer } from './infrastructure/fastify';
// import { startServer } from './infrastructure/express';

void startServer();
